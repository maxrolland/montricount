package com.maxiroll.montricount.mapper

import com.maxiroll.montricount.dto.UserDto
import com.maxiroll.montricount.services.beans.User
import org.mapstruct.Mapper

@Mapper
interface UserMapper {

    fun toDto(user: User): UserDto
    fun toBean(userDto: UserDto): User
}