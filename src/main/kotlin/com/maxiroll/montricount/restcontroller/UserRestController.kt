package com.maxiroll.montricount.restcontroller

import com.maxiroll.montricount.services.UserServices
import com.maxiroll.montricount.services.beans.User
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class UserRestController(private val userServices: UserServices) {
    @PostMapping(
        "/api/v1/user",
        consumes = [MediaType.APPLICATION_JSON_VALUE],
        produces = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun addCustomer(@RequestBody customerBean: User): ResponseEntity<out Any> {
        userServices.addNewUser(customerBean);
        return ResponseEntity.status(HttpStatus.OK).body("OK");
    }

}