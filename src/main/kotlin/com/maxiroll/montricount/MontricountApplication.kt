package com.maxiroll.montricount

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MontricountApplication

fun main(args: Array<String>) {
    runApplication<MontricountApplication>(*args)
}
