package com.maxiroll.montricount.dto

import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.Id
import jakarta.persistence.OneToMany

@Entity(name = "tricount")
data class TricountDto(
    @Id
    @GeneratedValue
    val identifiant: Long,
    @OneToMany
    val depenses: List<DepenseDto>,
    @OneToMany
    val participants: List<UserDto>
) {


}