package com.maxiroll.montricount.dto

import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.Id
import jakarta.persistence.Table

@Entity
@Table(name = "USERS")
data class UserDto(
    @Id
    @GeneratedValue
    val identifiant: Long,
    val fistName: String,
    val lastName: String,
    val email: String
) {}