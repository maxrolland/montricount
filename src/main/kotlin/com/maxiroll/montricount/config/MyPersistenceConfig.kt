package com.maxiroll.montricount.config

import jakarta.persistence.EntityManagerFactory
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.orm.jpa.JpaTransactionManager
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean
import javax.sql.DataSource

@Configuration
@EnableJpaRepositories(
    entityManagerFactoryRef = "myEntityManagerFactory",
    transactionManagerRef = "myTransactionManager"
)
class MyPersistenceConfig {

    @Bean
    fun myEntityManagerFactory(dataSource: DataSource): LocalContainerEntityManagerFactoryBean {
        val emfBean = LocalContainerEntityManagerFactoryBean()
        return emfBean
    }

    @Bean
    fun myTransactionManager(@Qualifier("myEntityManagerFactory") emf: EntityManagerFactory): JpaTransactionManager {
        val transactionManager = JpaTransactionManager()
        transactionManager.entityManagerFactory = emf
        return transactionManager
    }
}
