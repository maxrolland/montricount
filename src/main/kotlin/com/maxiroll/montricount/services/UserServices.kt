package com.maxiroll.montricount.services

import com.maxiroll.montricount.constantes.ErrorCode
import com.maxiroll.montricount.mapper.UserMapper
import com.maxiroll.montricount.repository.UserRepository
import com.maxiroll.montricount.services.beans.User
import org.mapstruct.factory.Mappers
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class UserServices(
    private val userRepository: UserRepository,
    private var userMapper: UserMapper = Mappers.getMapper(UserMapper::class.java)
) {

    @Transactional
    fun addNewUser(user: User) {
        if (userRepository.isValidateEmail(user.email) && userRepository.existsCustomerByEmail(user.email)) {
            userRepository.save(userMapper.toDto(user))
        }
    }

    sealed class ApiResult<out T> {
        internal data class Failure(val errorCode: ErrorCode, val errorMessage: String) : ApiResult<Nothing>()
        internal data class Success<T>(val value: T) : ApiResult<T>()
    }

}