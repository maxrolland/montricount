package com.maxiroll.montricount.repository

import com.maxiroll.montricount.dto.DepenseDto
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional

@Repository
@Transactional(propagation = Propagation.REQUIRED)
interface DepenseRepository : CrudRepository<DepenseDto, Long> {
}