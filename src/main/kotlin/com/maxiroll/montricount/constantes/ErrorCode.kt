package com.maxiroll.montricount.constantes

enum class ErrorCode {
    NOT_FOUND, INVALID_EMAIL, DUPLICATE_EMAIL, OTHER_ERROR
}