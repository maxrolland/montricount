package com.maxiroll.montricount.repository

import com.maxiroll.montricount.dto.UserDto
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Repository
@Transactional(propagation = Propagation.REQUIRED)
interface UserRepository : CrudRepository<UserDto, Long> {
    fun findByFirstNameAndLastName(firstName: String, lastName: String): Optional<UserDto>
    fun existsCustomerByEmail(email: String): Boolean
    fun isValidateEmail(email: String): Boolean {
        val emailRegex = Regex("[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}")
        return emailRegex.matches(email)
    }
}