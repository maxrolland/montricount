package com.maxiroll.montricount.config

import org.hibernate.jpa.HibernatePersistenceProvider
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean
import java.util.*
import javax.sql.DataSource


@Configuration
class AppConfig {
    @Bean
    fun h2DataSource(): DataSource {
        return EmbeddedDatabaseBuilder()
            .setType(EmbeddedDatabaseType.H2)
            .build()
    }

    @Bean
    fun factoryBean(): LocalContainerEntityManagerFactoryBean {
        val factory = LocalContainerEntityManagerFactoryBean()
        factory.dataSource = h2DataSource()
        factory.setPackagesToScan("com.maxiroll.montricount.dto");
        factory.setPersistenceUnitName("testdb");
        factory.setPersistenceProviderClass(HibernatePersistenceProvider::class.java)
        val properties = Properties()
        properties.setProperty("jakarta.persistence.schema-generation.database.action", "create")
        factory.setJpaProperties(properties)
        return factory
    }
}