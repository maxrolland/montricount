package com.maxiroll.montricount.repository

import com.maxiroll.montricount.dto.TricountDto
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional

@Repository
@Transactional(propagation = Propagation.REQUIRED)
interface TricountRepository : CrudRepository<TricountDto, Long> {
}