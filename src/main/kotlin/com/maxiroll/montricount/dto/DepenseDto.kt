package com.maxiroll.montricount.dto

import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.Id
import jakarta.persistence.ManyToMany


@Entity(name = "depenses")
data class DepenseDto(
    @Id
    @GeneratedValue
    val identifiant: Long,
    val montant: String,
    @ManyToMany
    val participants: List<UserDto>
) {}