package com.maxiroll.montricount.services.beans

data class User(
    val identifiant: Long,
    val fistName: String,
    val lastName: String,
    val email: String
) {}